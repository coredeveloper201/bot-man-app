<?php

namespace App\Http\Controllers;

use App\Question;
use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use App\Conversations\ExampleConversation;

class BotManController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');

	    $botman->hears('hello', function (BotMan $bot) {
		    $bot->reply('Hello yourself.');
	    });

	    $botman->hears('how are you|how are you?|how r u', function (BotMan $bot) {
		    $bot->reply('I am fine, thanks ! - how are you ?');
	    });

	    $botman->hears('who r u|who are you|who are you?', function (BotMan $bot) {
		    $bot->reply('I am BotMan, please tell me how may I help you.');
	    });

	    $botman->hears('Are you there?|are you there|r u there|there?|there', function (BotMan $bot) {
		    $bot->reply('Yes, I am here to help you.');
	    });

	    $botman->hears('{message}', function (BotMan $bot, $message) {
		    $answer = Question::where('question', $message)->pluck('answer')->toArray();
		    if($answer && count($answer)>0){
			    $bot->reply($answer[0]);
		    }elseif (preg_match('(hello|holla|new)', $message) === 1){
			    $bot->reply('Hello yourself.');
		    }else{
			    $bot->reply('Sorry, I did not understand - what did you say.');
		    }

	    });


	    $botman->fallback(function($bot) {
		    $bot->reply('Sorry, I did not understand - what did you say.');
	    });

	    $botman->listen();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        return view('tinker');
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new ExampleConversation());
    }

    public function sayHello(BotMan $bot){

    }

    public function handleReq(Request $request){
    	$input = $request->input;
    	dd($input);
    }
}
